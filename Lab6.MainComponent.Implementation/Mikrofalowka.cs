﻿using Lab6.Display.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Implementation
{
    public class Mikrofalowka : IMikrofalowka
    {
        IDisplay display;
        public Mikrofalowka(IDisplay display)
        {
            this.display = display;
        }

        public void ObracajTalerz()
        {
            Console.WriteLine("Obracam");
        }

        public string CoGotujesz(string posiłek)
        {
            return "gotujesz: " + posiłek;
        }
    }
}
