﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab6.Infrastructure;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Lab6.Infrastructure.Container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(Lab6.ControlPanel.Implementation.Adapter)); 

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Lab6.MainComponent.Contract.IMikrofalowka)); //interfejsik
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Lab6.MainComponent.Implementation.Mikrofalowka)); //majkrofala

        #endregion
    }
}
