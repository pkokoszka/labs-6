﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class Adapter : IControlPanel
    {

        IMikrofalowka mikrofalowka;
        Window window;

        public Adapter(IMikrofalowka mikrofalowka)
        {
            this.window = new FinalWindow(mikrofalowka);
            this.mikrofalowka = mikrofalowka;
        }

        public Window Window
        {
            get { return window; }
        }
    }
}
